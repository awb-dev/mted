prefix?=/usr/local

mted_cflags:=-std=c99 -Wall -Wextra -pedantic -Wno-pointer-arith -Wno-unused-result -Wno-unused-parameter -g -O0 -D_GNU_SOURCE -I. $(CFLAGS)
mted_ldflags:=$(LDFLAGS)
mted_dynamic_libs:=-lpcre -llua5.3
mted_static_libs:=vendor/pcre/.libs/libpcre.a vendor/lua/liblua5.3.a
mted_ldlibs:=-lm $(LDLIBS)
mted_objects:=$(patsubst %.c,%.o,$(wildcard *.c))
mted_objects_no_main:=$(filter-out main.o,$(mted_objects))
mted_func_tests:=$(wildcard tests/func/test_*.sh))
mted_unit_tests:=$(patsubst %.c,%,$(wildcard tests/unit/test_*.c))
mted_unit_test_objects:=$(patsubst %.c,%.o,$(wildcard tests/unit/test_*.c))
mted_unit_test_all:=tests/unit/test
mted_vendor_deps:=
mted_static_var:=

ifdef mted_static
  mted_static_var:=-static
endif

ifdef mted_vendor
  mted_ldlibs:=$(mted_static_libs) $(mted_ldlibs)
  mted_cflags:=-Ivendor/pcre -Ivendor -Ivendor/uthash/src $(mted_cflags)
  mted_vendor_deps:=$(mted_static_libs)
else
  mted_ldlibs:=$(mted_dynamic_libs) $(mted_ldlibs)
endif

all: mted

mted: $(mted_vendor_deps) $(mted_objects) termbox2.h
	$(CC) $(mted_static_var) $(mted_cflags) $(mted_objects) $(mted_ldflags) $(mted_ldlibs) -o mted

$(mted_objects): %.o: %.c
	$(CC) -c $(mted_cflags) $< -o $@

$(mted_vendor_deps):
	$(MAKE) -C vendor

$(mted_unit_test_objects): %.o: %.c
	$(CC) -DTEST_NAME=$(basename $(notdir $<)) -c $(mted_cflags) -rdynamic $< -o $@

$(mted_unit_test_all): $(mted_objects_no_main) $(mted_unit_test_objects) $(mted_unit_test_all).c
	$(CC) $(mted_cflags) -rdynamic $(mted_unit_test_all).c $(mted_objects_no_main) $(mted_unit_test_objects) $(mted_ldflags) $(mted_ldlibs) -ldl -o $@

$(mted_unit_tests): %: $(mted_unit_test_all)
	{ echo "#!/bin/sh"; echo "$(abspath $(mted_unit_test_all)) $(notdir $@)"; } >$@ && chmod +x $@

test: mted $(mted_unit_tests)
	./mted -v && export mted=$$(pwd)/mted && $(MAKE) -C tests

sloc:
	find . -name '*.c' -or -name '*.h' | \
		grep -Pv '(termbox|test|ut|lua)' | \
		xargs -rn1 cat | \
		wc -l

install: mted
	install -v -d $(DESTDIR)$(prefix)/bin
	install -v -m 755 mted $(DESTDIR)$(prefix)/bin/mted

uscript:
	php uscript.inc.php >uscript.inc

clean_quick:
	rm -f mted $(mted_objects) $(mted_unit_test_objects) $(mted_unit_tests) $(mted_unit_test_all)

clean:
	rm -f mted $(mted_objects) $(mted_vendor_deps) $(mted_unit_test_objects) $(mted_unit_tests) $(mted_unit_test_all)
	$(MAKE) -C vendor clean

.PHONY: all test sloc install uscript clean
